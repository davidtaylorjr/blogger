This is a simple blog applicaiton created for The Odin Project.

It has the ability to create/edit/delete articles and users with full password authentication.

The application is written in Ruby on Rails.
